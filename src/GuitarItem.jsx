const GuitarItem = (props) => {

    let hasGuitarBrand = false

    if (props.brand) // !empty -> "", !undefined, !null
        hasGuitarBrand = true

    return (
        <div>
            {props.brand ? <div>
                <h3>{props.brand}</h3>
                <small>{props.id}</small>
                <p>{props.price}</p>
            </div> : <p>No guitar data to display</p>}
        </div>
    )
}

export default GuitarItem

// JavaScript ternary operator / ternary condition
// syntax:
// let myBoolean = 3 < 5 ? "true" : "false"

// if (3 < 5)
//     myBoolean = "true"
// else
//     myBoolean = "false"