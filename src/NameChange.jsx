import { useState } from 'react'
import './NameChange.css'

const NameChange = () => {
    
    // (REACT) LIFECYCLE HOOKS = Class Components (obsolete)
    // REACT HOOKS = Functional Components
    const [name, setName] = useState("Warren") // immutable!

    //let name = "Warren"
    
    // handle button click
    function handleChangeNameClick() {
        setName("Sean")        

        console.log("Button works! 😺");
        console.log("New name", name);
    }

    return(
        <div>
            <p>{ name }</p>
            <button onClick={handleChangeNameClick}>Click me!</button>
        </div>
    )
}

export default NameChange


// let [myFirstName, mySecondName] = myLittleFunction()

// function myLittleFunction() {
//     return ["Warren", "West"]
// }