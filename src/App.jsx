import './App.css';
import GuitarItem from './GuitarItem'
import NameChange from './NameChange'

function App() {

  const guitars = [
    {
      brand: undefined,
      price: 19.99,
      id: 1,
    }, 
    {
      brand: "Gibson",
      price: 39.99,
      id: 2,  
    }, 
    {
      brand: "Epiphone",
      price: 79.99,
      id: 3,
    }
  ]

  // Array.Prototype.map()
  function renderGuitars() {
    const guitarItems = guitars.map((guitar) => {
      return <GuitarItem brand={guitar.brand} id={guitar.id} price={guitar.price} />
    })
    return guitarItems
  }

  return (
    <div className="App">
      Our first React app!
      {/* { renderGuitars() } */}
      <NameChange />
    </div>
  );
}

export default App;