function MyFirstComponent(props) {
        
    console.log(props);

    return (
        <div>
            <h3>{ props.heading }</h3>
            <h4>{ props.subHeading }</h4>
            <p>{ props.paragraph }</p>
        </div>
    )
}
export default MyFirstComponent