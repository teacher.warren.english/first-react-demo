# First-React
A little description about the project.

## Using
To run this demo application, you need to install the dependencies, as the `node_modules` folder does not exist on the git repository. First run the following command to clone the repo:
```
git clone https://gitlab.com/teacher.warren.english/first-react-demo.git
```

Then use the following command to install the dependencies (this will create a `node_modules` folder):
```
npm install
```

## Contributors
@Warren-West | Noroff Accelerate AS

## License
MIT